#pragma once
#include "Product.h"

enum class NonperishableProductType
{
	Clothing,
	SmallAppliences,
	PersonalHygiene
};

class NonperishableProduct :public Product
{
public:
	NonperishableProduct(int32_t id, const std::string& name, float rawPrice, const NonperishableProductType& type);

	NonperishableProductType getType() const;
	float getPrice() const final;
	int32_t getVAT() const final;

private:
	NonperishableProductType m_type;
};